# Readme
### TODO

###### Core:
* Create main Frame
	* Menu Bar
		* File
			* New
			* ~~Save~~
			* ~~Open~~
			* Generate code
				* ~~C++~~
				* Java
		* Edit
			* ~~New Class~~
	* ~~Add UML Objects~~
	* Popup Menu
		* ~~New Class~~
		* ~~Paste~~
		
###### Add Class:
* New Class dialog
	* ~~Set Class Name~~
	* ~~Set Super Class Name~~
	* ~~Set Class Description~~
	* ~~Add Class Members~~
	
###### Add Class Member:
* Add Class Members dialog
	* ~~Set Member Name~~
	* ~~Set Member Description~~
	* ~~Set Member Access Type~~
	
###### UML Container:
* Panel inside main Frame
	* Content
		* ~~Displays Class Name~~
		* ~~Displays Class Super Name~~
		* ~~Displays Class public Members~~
		* ~~Displays Class private Members~~
		* ~~Displays Class protected Members~~
	* ~~Draggable~~
	* Resizeable
		* ~~On content change~~
		* Manually
	* Connectors //maybe use Bezier curves, curveTo
		* Lines
		* Check Access Permissions
	* Popup Menu
		* ~~Edit~~
		* ~~Copy~~
		* ~~Delete~~
		
###### General:
* Cleanup code
* Better format GUI dialogs
* Check that creating a new class uses a unique class name
* Check that a super name has an existing class

* Clear uml's before opening a new one

redraw line boxes on screen resize

[Markup examples](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
