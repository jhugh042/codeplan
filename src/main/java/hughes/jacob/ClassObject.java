package hughes.jacob;

import javax.swing.DefaultListModel;

public class ClassObject {

	private String className,superName,classDesc;
	private DefaultListModel<MemberObject> publicMem,protectedMem, privateMem;
	private int x =0,y=0;
	ClassObject(){
		
	}
	ClassObject(String _class,String _super, String _desc,
			DefaultListModel<MemberObject> _public,DefaultListModel<MemberObject> _private,DefaultListModel<MemberObject> _protected){
		className = _class;
		superName = _super;
		classDesc = _desc;
		publicMem = _public;
		privateMem = _private;
		protectedMem = _protected;
	}
	
	public void setXY(int _x, int _y) {
		x = _x;
		y = _y;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public String getSuperName() {
		return superName;
	}
	
	public String getClassName() {
		return className;
	}
	public String getClassDescription() {
		return classDesc;
	}
	public DefaultListModel<MemberObject> getPublicMembers() {
		return publicMem;
	}
	public DefaultListModel<MemberObject> getPrivateMembers() {
		return privateMem;
	}
	public DefaultListModel<MemberObject> getProtectedMembers() {
		return protectedMem;
	}
}
