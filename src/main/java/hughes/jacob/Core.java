package hughes.jacob;


import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * ======================================================
 * @Author: Jacob Hughes
 * Old Dominion Univeristy
 *///======================================================
public class Core {

	//the static variables to be used
	public static Color background = Color.WHITE;
	public static Color borderColor = Color.BLACK;
	public static Border emptyBorder =  BorderFactory.createEmptyBorder(2,10,10,10);
	public static Border coloredBorder =  BorderFactory.createMatteBorder(2, 2, 2, 2, borderColor);
	public static Border underlineBorder = BorderFactory.createMatteBorder(0, 0, 2, 0, borderColor);
	public static Color listSelectionBackground = Color.CYAN;
	public static Color listSelectionForeground = Color.BLACK;
	public static Color listNotSelectedBackground = Color.WHITE;
	public static Color listNotSelectedForeground = Color.BLACK;
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		GUI gui = new GUI();
	}
	
	/**
	 * global function for creating a new class object
	 * @param classObject
	 * @return classobject
	 */
	public static ClassObject createClass(ClassObject classObject) {
		
		//initialize the data for createClass JPanel
		JTextField className = new JTextField(10);
		JTextField superName = new JTextField(10);
		JTextField classDesc = new JTextField(10);
		DefaultListModel<MemberObject> publicMem = new DefaultListModel<MemberObject>();
		DefaultListModel<MemberObject> protectedMem = new DefaultListModel<MemberObject>();
		DefaultListModel<MemberObject> privateMem = new DefaultListModel<MemberObject>();
		if(classObject!=null) {
			className.setText(classObject.getClassName());
			superName.setText(classObject.getSuperName());
			classDesc.setText(classObject.getClassDescription());
			publicMem = classObject.getPublicMembers();
			privateMem = classObject.getPrivateMembers();
			protectedMem = classObject.getProtectedMembers();
		}
		
		//creates JPanel for the dialog
		JPanel createClass = new NewClass(className,superName,classDesc,publicMem,protectedMem,privateMem);
		
		//display the dialog
		int result = JOptionPane.showConfirmDialog(null, 
				createClass, "Create a class",JOptionPane.OK_CANCEL_OPTION);
		
		//if ok is selected
		if(result == JOptionPane.OK_OPTION) {
			//error if no class name is given
			if(className.equals("")) {
				JOptionPane.showMessageDialog(null, "Class Name cannot be empty", "Error", JOptionPane.ERROR_MESSAGE);
			}
			else {
				classObject = new ClassObject(className.getText(),superName.getText(),
										classDesc.getText(),publicMem,privateMem,protectedMem);
			}
		}
		return classObject;
	}
}
