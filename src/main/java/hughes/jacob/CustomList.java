/*======================================================
 * Author: Jacob Hughes
 * Old Dominion Univeristy
 *///======================================================
package hughes.jacob;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/*
 * creates custom renderer for JList so that 
 * MemberObject displays the member name
 */
@SuppressWarnings({ "serial", "rawtypes" })
public class CustomList extends JLabel implements ListCellRenderer {

	boolean hover = false;
	Color color;
	public CustomList() {
		this.setOpaque(true);
	}
	
	public CustomList(boolean _hover, Color _color) {
		this.setOpaque(true);
		hover = _hover;
		color = _color;
	}
	
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		
		MemberObject member = (MemberObject) value;
		setText(member.getBaseName());
		this.setOpaque(true);
		
		setForeground(Core.listSelectionForeground);
		
		if(!hover) {
	        if (isSelected) {
	            setBackground(Core.listSelectionBackground);
	        } else {
	            setBackground(Core.listNotSelectedBackground);
	        }
		}
		else{
			setBackground(color);
		}
        
		return this;
	}
	
	public void updateBG(Color color) {
		this.setOpaque(true);
		this.setBackground(color);
	}

}
