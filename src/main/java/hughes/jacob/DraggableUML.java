package hughes.jacob;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Creates a UML object that contains the class information
 * 
 * @author Jake M Hughes
 * Old Dominon University
 *
 */
@SuppressWarnings("serial")
public class DraggableUML extends JPanel{

	  //varriables used for the drag element
	  private volatile int screenX = 0;
	  private volatile int screenY = 0;
	  private volatile int myX = 0;
	  private volatile int myY = 0;
	  
	  //variables to display class information
	  private JLabel className,publicMem,protectedMem,privateMem;
	  private ClassObject classObject;
	  private DefaultListModel<MemberObject> publicMembersArray,protectedMembersArray, privateMembersArray;
	  private JList<MemberObject> pubMemList, protMemList, privMemList;
	  private CustomList pubCList,privCList,protCList;

	  //the creator GUI
	  GUI parent;
	  
	  /**
	   * constructor uses class data to generate new UML and x,y for position
	   * 
	   * @param gui
	   * @param _classObject
	   * @param x
	   * @param y
	   */
	  public DraggableUML(GUI gui,ClassObject _classObject,int x,int y) {
		  parent = gui;
		  
		  classObject = _classObject;
		  if(_classObject.getSuperName().equals("") || _classObject.getSuperName().equals(null)) {
			  className = new JLabel(_classObject.getClassName());
		  }else {
			  className = new JLabel(_classObject.getClassName()+":"+_classObject.getSuperName());
		  }
		  publicMembersArray = _classObject.getPublicMembers();
		  privateMembersArray = _classObject.getPrivateMembers();
		  protectedMembersArray = _classObject.getProtectedMembers();
		  setLocation(x,y);
		  setGUI();
	  }
	  
	  /**
	   * gets this object's class information
	   * @return ClassObject
	   */
	  public ClassObject getClassObject() {
		  return classObject;
	  }
	  
	  /**
	   * gets this object, used for nested classes
	   * @return DraggableUML
	   */
	  private DraggableUML getThis() {
		  return this;
	  }
	  
	  /**
	   * sets the color of the members, used for hover effect
	   * @param color
	   */
	  @SuppressWarnings("unchecked")
	  public void setMembersColor(Color color,boolean protM) {
		  pubMemList.setCellRenderer(new CustomList(true,color));
		  if(protM) {
			  protMemList.setCellRenderer(new CustomList(true,color));
		  }
	  }
	  
	  /*
	   * sets GUI elements for draggableUML component
	   */
	  @SuppressWarnings("unchecked")
	  private void setGUI() {
		  
		GridLayout grid = new GridLayout(0,1);
		setLayout(grid);
		pubCList = new CustomList();
		protCList = new CustomList();
		privCList = new CustomList();
		//initialize the JLists
		pubMemList = new JList<MemberObject>(publicMembersArray);
		protMemList = new JList<MemberObject>(protectedMembersArray);
		privMemList = new JList<MemberObject>(privateMembersArray);
		
		pubMemList.setEnabled(false);
		protMemList.setEnabled(false);
		privMemList.setEnabled(false);
			
		//make it so the list only displays the member name of the object
		pubMemList.setCellRenderer(pubCList);
		protMemList.setCellRenderer(protCList);
		privMemList.setCellRenderer(privCList);
		
		publicMem = new JLabel("public:");
		protectedMem = new JLabel("protected:");
		privateMem = new JLabel("private:");
		
		className.setBorder(Core.underlineBorder);
		
		add(className);
		add(publicMem);
		add(pubMemList); 
		add(protectedMem);
		add(protMemList);
		add(privateMem);
		add(privMemList);
		setBorder(new CompoundBorder(
			    Core.coloredBorder, 
			    Core.emptyBorder));
		setBackground(Core.background);
		
		setSize(this.getPreferredSize());
		setOpaque(false);
		addMouseListener(new ThisMouseListner());
		//addListeners();
		addMouseMotionListener(new ThisMouseListner());
		setVisible(true);
	  }
	  
	  /**
	   * after an edit change has been made calls update this to update the UML values
	   * @param _classObject
	   */
	  private void updateThis(ClassObject _classObject) {
		  
		  classObject = _classObject;
		  if(_classObject.getSuperName().equals("") || _classObject.getSuperName().equals(null)) {
			  className.setText(_classObject.getClassName());
		  }else {
			  className.setText(_classObject.getClassName()+":"+_classObject.getSuperName());
		  }
		  publicMembersArray = _classObject.getPublicMembers();
		  privateMembersArray = _classObject.getPrivateMembers();
		  protectedMembersArray = _classObject.getProtectedMembers();
		  
		  setSize(this.getPreferredSize());
		  revalidate();
	  }
	  
	  /**
	   * mouse listener for uml objects
	   * 
	   * @author Jake M Hughes
	   */
	  class ThisMouseListner extends MouseAdapter implements MouseMotionListener{
		  
		  /**
		   * class for the popup menu for UML objects
		   * @author Jake M Hughes
		   *
		   */
		  class PopUp extends JPopupMenu{
			  
			  //the variables to be used for popup
			  JMenuItem itemEdit,itemCopy,itemDelete, itemLink;
			  String commandLink = "Create Link";
			  String commandEdit = "Edit";
			  String commandCopy = "Copy";
			  String commandDelete = "Delete";
			  
			  /**
			   * popup constructor that initializes the data
			   */
			  PopUp(){
				  //creates the menu variables
				  itemEdit = new JMenuItem(commandEdit);
				  itemCopy = new JMenuItem(commandCopy);
				  itemDelete = new JMenuItem(commandDelete);
				  itemLink = new JMenuItem(commandLink);
				  
				  //adds the action listeners
				  itemEdit.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						  ClassObject _classObject = Core.createClass(getClassObject());
						  updateThis(_classObject);
					}
				  });
				  itemCopy.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						parent.clipboard = classObject;
					}
				  });
				  itemDelete.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						parent.removeUML(getThis());
					}
				  });
				  itemLink.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						//if the arry is empty theres nothing to link,
						//if the arry only has one element, it cant link itself
						if(parent.getUMLs().isEmpty() || parent.getUMLs().size() <2) {
							
							JOptionPane.showMessageDialog(null, "Nothing to link", "Error", JOptionPane.ERROR_MESSAGE);
							
						}else {
							
							//converts the array to a default list model
							DefaultListModel<DraggableUML> arry = new DefaultListModel<DraggableUML>();
							for(DraggableUML objs : parent.getUMLs()) {
								arry.addElement(objs);
							}
							
							//creates the information for the option pane
							DraggableUML selected = null;
							ConnectorLink linker = new ConnectorLink(arry,selected);
							int result = JOptionPane.showConfirmDialog(null, 
									linker, "Select a class to link",JOptionPane.OK_CANCEL_OPTION);
							
							//checks the result
							if(result == JOptionPane.OK_OPTION) {
								selected = linker.getUMLObj();
								//if not null create the connection
								if(selected!=null) {
									parent.addLine(new LineObject(getThis(),selected));
								}else {
									JOptionPane.showMessageDialog(null, "You didnt select anything.", "Error", JOptionPane.ERROR_MESSAGE);
								}
							}
						}
					}
				  });

				  //adds items to the menu
				  add(itemLink);
				  this.addSeparator();
				  add(itemEdit);
				  add(itemCopy);
				  add(itemDelete);
			  }
		  }

		  /**
		   * handles the code for dragging the UML
		   * @param MouseEvent e
		   */
		  public void mouseDragged(MouseEvent e) {
			  int deltaX = e.getXOnScreen() - screenX;
			  int deltaY = e.getYOnScreen() - screenY;

			  setLocation(myX + deltaX, myY + deltaY);
			  
			  for(LineObject object : parent.getLines()) {
				  if(object.getSource().equals(getThis())){
					  object.setX1(myX+deltaX);
					  object.setY1(myY+deltaY);
				  }
				  else if(object.getPointsTo().equals(getThis())){
					  object.setX2(myX+deltaX);
					  object.setY2(myY+deltaY);
				  }
				  parent.reDraw();
			  }
			  
		  }
		  
		  /**
		   * handles the code for stop hovering over the UML
		   * @param MouseEvent e
		   */
		  public void mouseExited(MouseEvent e) {
			  for(LineObject obj : parent.getLines()) {
				  if(obj.getSource().equals(getThis())) {
					  DraggableUML pointUML = obj.getPointsTo();
					  pointUML.setMembersColor(Color.WHITE,true);
				  }
			  }
		  }
		  
		  /**
		   * handles the code for hovering over the uml
		   * @param MouseEvent e
		   */
		  public void mouseEntered(MouseEvent e) {
			  for(LineObject obj : parent.getLines()) {
				  if(obj.getSource().equals(getThis())) {
					  DraggableUML pointUML = obj.getPointsTo();
					  ClassObject classObj = pointUML.classObject;
					  if(classObj.getClassName().equals(getThis().classObject.getSuperName())){
						  pointUML.setMembersColor(Color.GREEN,true);
					  }
					  else {
						  pointUML.setMembersColor(Color.GREEN,false);
					  }
				  }
			  }
		  }

		  /**
		   * handles the code for creating popup window
		   * @param MouseEvent e
		   */
		  public void mousePressed(MouseEvent e) {
			  if(e.isPopupTrigger()) {
				  doPopup(e);
			  }
			  else {
				  screenX = e.getXOnScreen();
				  screenY = e.getYOnScreen();

				  myX = getX();
				  myY = getY();
			  }
		  }
		  
		  public void mouseReleased(MouseEvent e){
		        if (e.isPopupTrigger())
		            doPopup(e);
		    }
		  
		  private void doPopup(MouseEvent e) {
			  PopUp pop = new PopUp();
			  pop.show(e.getComponent(), e.getX(), e.getY());
		  }
	  }
	  
	  /**
	   * handles the code for creating a link between two objects
	   * @author Jake M Hughes
	   */
	  private class ConnectorLink extends JPanel{
		  JList<DraggableUML> list;
		  private DraggableUML valselected;
		  
		  /**
		   * 
		   * @param umls
		   * @param _selected
		   */
		  ConnectorLink(DefaultListModel<DraggableUML> umls, DraggableUML _selected){
			  list = new JList<DraggableUML>(umls);
			
			  valselected = _selected;
			  setCGUI();
		  }
		  
		  private void setCGUI() {
			  list.addListSelectionListener(new ListSelectionListener() {

				@Override
				public void valueChanged(ListSelectionEvent e) {
					valselected = list.getSelectedValue();
					System.out.println(valselected.toString());
				}
				  
			  });
			  this.add(list);
		  }
		  public DraggableUML getUMLObj() {
			  return valselected;
		  }
	  }
	  
	  /**
	   * toString overide, needed for the JList objects
	   * @return String : classname
	   */
	  @Override
	  public String toString() {
		  if(classObject.getSuperName().equals(null) || classObject.getSuperName().equals("")) {
			  return classObject.getClassName();
		  }
		  else {
			  return classObject.getClassName()+":"+classObject.getSuperName();
		  }
	  }
}
