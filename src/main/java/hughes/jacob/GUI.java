package hughes.jacob;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;


/**
 * the main frame of the program
 * holds menu data and uml boxes
 * @author Jake M Hughes
 * Old Dominion University
 *
 */
@SuppressWarnings("serial")
public class GUI extends JFrame{

	final String windowName = "Code Planner";
	JFrame frame;
	JMenuBar menuBar;
	
	ClassObject clipboard=null;
	
	ArrayList<DraggableUML> array;
	Line line;
	
	/**
	 * GUI constructor the initializes everything
	 */
	GUI(){
		array = new ArrayList<>();
		line = new Line();
		menuBar = new ThisMenuController();
		
		addMouseListener(new ThisMouseListener());
		setBackground(Core.background);
		setLayout(null);
		setJMenuBar(menuBar);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 1000);
		this.setBounds(0,0,1000,1000);
		add(line);
		setVisible(true);
	}
	
	/**
	 * adds the uml to the GUI screen
	 * @param _classObject
	 * @param x
	 * @param y
	 */
	private void addUML(ClassObject _classObject,int x,int y) {
		
		DraggableUML uml = new DraggableUML(this,_classObject,x,y);
		array.add(uml);
		add(uml);
		remove(line);
		add(line);
		reDraw();
		
	}
	
	/**
	 * returns the umls currently dran to the screen
	 * @return ArrayList<DraggableUML> umls
	 */
	public ArrayList<DraggableUML> getUMLs(){
		return array;
	}
	
	/**
	 * adds the line object to GUI to be drawn
	 * @param lineObj
	 */
	public void addLine(LineObject lineObj) {
		line.addLine(lineObj);
		reDraw();
	}
	
	/**
	 * repaints and revalidates the GUI screen
	 */
	public void reDraw() {
		repaint();
		revalidate();
	}
	
	/**
	 * removes the selected UML from GUI
	 * @param uml
	 */
	public void removeUML(DraggableUML uml) {
		remove(uml);
		array.remove(uml);
		reDraw();
	}
	
	/**
	 * returns the list of LineObjects drawn
	 * @return ArrayList<LineObject> lines
	 */
	public ArrayList<LineObject> getLines(){
		return line.getLines();
	}
	
	/**
	 * creates the menu bar component and sets the action commands
	 * @author Jake M Hughes
	 */
	private class ThisMenuController extends JMenuBar implements ActionListener{

		JMenu file,edit,generateCode;
		
		final String[] fileCommands = {"New", "Save", "Open...", "Exit"};
		final String[] editCommands = {"New Class"};
		final String[] generateCommands = {"C++","Java"};
		
		public ThisMenuController() {
			
			//create generate Code menu and adds sub items
			generateCode = new JMenu("Generate Code");
			for(int i=0;i<generateCommands.length;i++) {
				JMenuItem currentItem = new JMenuItem(generateCommands[i]);
				currentItem.setActionCommand(generateCommands[i]);
				currentItem.addActionListener(this);
				generateCode.add(currentItem);
			}
			
			//create file menu and adds sub items
			file = new JMenu("File");
			for(int i=0;i<fileCommands.length;i++) {
				
				if(fileCommands[i].equals("Exit")) {
					file.add(generateCode);
					file.addSeparator();
				}
				
				JMenuItem currentItem = new JMenuItem(fileCommands[i]);
				currentItem.setActionCommand(fileCommands[i]);
				currentItem.addActionListener(this);
				file.add(currentItem);
			}
			
			//create Edit menu and adds sub items
			edit = new JMenu("Edit");
			for(int i=0;i<editCommands.length;i++) {
				JMenuItem currentItem = new JMenuItem(editCommands[i]);
				currentItem.setActionCommand(editCommands[i]);
				currentItem.addActionListener(this);
				edit.add(currentItem);
				
			}
			
			add(file);
			add(edit);
		}
		

		public void actionPerformed(ActionEvent e) {
			//handles the action for Add Class
			if(editCommands[0].equals(e.getActionCommand())) {

				ClassObject classObject =null;
				classObject = Core.createClass(classObject);
				
				if(classObject!=null) {
					addUML(classObject,0,0);
				}
				
			}//handles the New Command
			else if(fileCommands[0].equals(e.getActionCommand())) {
				//TODO
				
			}//handles the action to save the project
			else if(fileCommands[1].equals(e.getActionCommand())) {
				//only save if a uml object exists
				if(!array.isEmpty()) {
					new Save(array);
				}
				else {
					JOptionPane.showMessageDialog(null, "Nothing to save!", "Error", JOptionPane.OK_OPTION);
				}
				
				
			}//handles the action to open a project from file system
			else if(fileCommands[2].equals(e.getActionCommand())) {
				//only ask to save if a uml object exists
				if(!array.isEmpty()) {
					int result = JOptionPane.showConfirmDialog(null, "Would you like to save before creating new project?", "Save?", JOptionPane.YES_NO_CANCEL_OPTION);
					
					//save if yes is chosen
					if(result == JOptionPane.YES_OPTION) {
						new Save(array);
						
					}
					
					//dont save if no is chosen
					if(!(result == JOptionPane.CANCEL_OPTION)) {
						Open open = new Open();
						for(ClassObject obj : open) {
							addUML(obj,obj.getX(),obj.getY());
						}
					}
					
				}//dont ask to save if no uml exists
				else {
					Open open = new Open();
					for(ClassObject obj : open) {
						addUML(obj,obj.getX(),obj.getY());
					}
				}
			}//handles c++ code generator
			else if(generateCommands[0].equals(e.getActionCommand())) {
				
				new GenerateCode(array,"C++");
				
			}//handles Java cod Generator
			else if(generateCommands[1].equals(e.getActionCommand())) {
				new GenerateCode(array,"Java");
			}
		}
	}
	
	/*
	 * mouse listener for gui
	 */
	class ThisMouseListener extends MouseAdapter{
		
		//popup menu for right clicking the jframe 
		class PopUp extends JPopupMenu{
			  JMenuItem itemClass,itemPaste;
			  String commandClass = "New Class";
			  String commandPaste = "Paste";
			  //creates popup menu items
			  PopUp(int x, int y){
				  itemClass = new JMenuItem(commandClass);
				  itemPaste = new JMenuItem(commandPaste);
				  
				  if(clipboard==null) {
					  itemPaste.setEnabled(false);
				  }
				  //action listener for pasting a uml object
				  itemPaste.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						addUML(clipboard,x,y);
					}  
				  });
				  //action listener for creating a new UML object
				  itemClass.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						ClassObject classObject = Core.createClass(null);
						if(classObject!=null) {
							addUML(classObject,x,y);
						}
					}
					  
				  });
				  
				  add(itemClass);
				  add(itemPaste);
			  }
		  }
		
		  public void mousePressed(MouseEvent e) {
			  if(e.isPopupTrigger()) {
				  doPopup(e);
			  }
		  }
		  
		  public void mouseReleased(MouseEvent e){
		        if (e.isPopupTrigger())
		            doPopup(e);
		    }
		  
		  private void doPopup(MouseEvent e) {
			  PopUp pop = new PopUp(e.getX(),e.getY());
			  pop.show(e.getComponent(), e.getX(), e.getY());
		  }
	}
	
}
