package hughes.jacob;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.JFileChooser;

@SuppressWarnings("serial")
public class GenerateCode{

	GenerateCode(ArrayList<DraggableUML> uml,String type){
		
		JFileChooser fileChooser = new JFileChooser();
	    // Demonstrate "Open" dialog:
		//fileChooser.setFileFilter(new FileNameExtensionFilter("XML Document","xml"));
		
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
	    int result = fileChooser.showSaveDialog(null);
		String filename;
	    if (result == JFileChooser.APPROVE_OPTION) {
	    	filename = fileChooser.getCurrentDirectory().toString()+"\\"
	    				+fileChooser.getSelectedFile().getName();
	    	
	    	if(type.equals("C++")) {
	    		
	    		try {
					cpp(uml,filename);
				} catch (IOException e) {
					e.printStackTrace();
				}	
	    	}
	    	else if(type.equals("Java")){
	    		
	    		try {
					java(uml,filename);
				} catch (IOException e) {
					e.printStackTrace();
				}
	    	}
	    	
	    	System.out.println(filename);
	    }
		
	}
	
	private void writeLine(FileOutputStream fos,String message) {
		
		message = message+"\n";
		try {
			fos.write(message.getBytes());
			fos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void writeComment(FileOutputStream fos,String message) {
		
		message = "* "+message+"\n";
		try {
			writeLine(fos,"/*");
			fos.write(message.getBytes());
			writeLine(fos,"*/ \n");
			fos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void cpp(ArrayList<DraggableUML> _uml,String fileLoc) throws IOException {
		for(DraggableUML uml : _uml) {
			
			ClassObject object = uml.getClassObject();
			
			//header file
			String filename = fileLoc+"\\"+object.getClassName();
			System.out.println(filename);
				
			FileOutputStream fos = new FileOutputStream(filename+".h");
			
			//write define statements
			writeLine(fos, "#ifndef _"+object.getClassName().toUpperCase());
			writeLine(fos,"#define _"+object.getClassName().toUpperCase());
			writeLine(fos,"");
			//write includes
			
			writeLine(fos,"");
			
			//write class
			writeComment(fos,object.getClassDescription());
			if(object.getSuperName().equals("")) {
				writeLine(fos,"class "+object.getClassName()+" {");
			}
			else {
				writeLine(fos,"class "+object.getClassName()+"::"
									+object.getSuperName()+" {");
			}
			writeLine(fos,"");
			
			//write public members
			writeLine(fos,"public:");
			for( Object member : Arrays.asList(object.getPublicMembers().toArray())) {
				writeLine(fos,"\t"+((MemberObject) member).getName()+";");
			}
			writeLine(fos,"");
			
			//write protected members
			writeLine(fos,"protected:");
			for( Object member : Arrays.asList(object.getProtectedMembers().toArray())) {
				writeLine(fos,"\t"+((MemberObject) member).getName()+";");
			}
			writeLine(fos,"");
			
			//write private members
			writeLine(fos,"private:");
			for( Object member : Arrays.asList(object.getPrivateMembers().toArray())) {
				writeLine(fos,"\t"+((MemberObject) member).getName()+";");
			}
			writeLine(fos,"");
			
			//closing
			writeLine(fos,"};");
			writeLine(fos,"#endif");
			fos.close();
			
			//open new fos for .cpp file
			fos = new FileOutputStream(filename+".cpp");
			
			//includes
			writeLine(fos,"#include \""+object.getClassName()+".h\"");
			writeLine(fos,"");
			
			//member definitions
			for( Object member : Arrays.asList(object.getPublicMembers().toArray())) {
				
				MemberObject temp = (MemberObject) member;
				if(temp.getName().contains("(")) {
					writeComment(fos,temp.getDescription());
					
					String type="";
					String name="";
					String msg = "ERROR";
					//if constructor
					if(temp.getName().contains(object.getClassName())) {
						msg = object.getClassName()+"::"+temp.getName()+" {";
					}
					else {
						int loc = temp.getName().indexOf(" ");
						type = temp.getName().substring(0, loc);
						name = temp.getName().substring(loc, temp.getName().length());
						msg = type+" "+object.getClassName()+"::"+name+" {";
					}
					writeLine(fos,msg);
					writeLine(fos,"");
					writeLine(fos,"}");
					writeLine(fos,"");
				}
			}
			
			//member definitions
			for( Object member : Arrays.asList(object.getProtectedMembers().toArray())) {
				
				MemberObject temp = (MemberObject) member;
				if(temp.getName().contains("(")) {
					writeComment(fos,temp.getDescription());
					
					String type="";
					String name="";
					String msg = "ERROR";
					//if constructor
					if(temp.getName().contains(object.getClassName())) {
						msg = object.getClassName()+"::"+temp.getName()+" {";
					}
					else {
						int loc = temp.getName().indexOf(" ");
						type = temp.getName().substring(0, loc);
						name = temp.getName().substring(loc, temp.getName().length());
						msg = type+" "+object.getClassName()+"::"+name+" {";
					}
					writeLine(fos,msg);
					writeLine(fos,"");
					writeLine(fos,"}");
					writeLine(fos,"");
				}
			}
			
			//member definitions
			ArrayList<MemberObject> val = new ArrayList<MemberObject>();
			for( Object member : Arrays.asList(object.getPrivateMembers().toArray())) {
				val.add((MemberObject) member);
			}
			Collections.sort(val);
			for( MemberObject member : val) {
				
				if(member.getName().contains("(")) {
					writeComment(fos,member.getDescription());
					
					String type="";
					String name="";
					String msg = "ERROR";
					//if constructor
					if(member.getName().contains(object.getClassName())) {
						msg = object.getClassName()+"::"+member.getName()+" {";
					}
					else {
						int loc = member.getName().indexOf(" ");
						type = member.getName().substring(0, loc);
						name = member.getName().substring(loc-1, member.getName().length());
						msg = type+" "+object.getClassName()+"::"+name+" {";
					}
					writeLine(fos,msg);
					writeLine(fos,"");
					writeLine(fos,"}");
					writeLine(fos,"");
				}
			}
			
			fos.close();
		}
	}
	
	private void java(ArrayList<DraggableUML> _uml,String fileLoc) throws IOException {
		for(DraggableUML uml : _uml) {
			ClassObject object = uml.getClassObject();
			
			String filename = fileLoc+"\\"+object.getClassName();
			
			/*
			FileOutputStream fos = new FileOutputStream(filename+".java");
			
			if(object.getSuperName().equals("")) {
				writeLine(fos,"public class "+object.getClassName()+" { ");
			}
			else {
				writeLine(fos,"public class "+object.getClassName()
							+" extends "+object.getSuperName()+" { ");
			}
			
			//TODO
			//needs to print normal variables and then functions
			*/
			
			//member definitions
			ArrayList<MemberObject> val = new ArrayList<MemberObject>();
			for( Object member : Arrays.asList(object.getPrivateMembers().toArray())) {
				//TODO make sure non functions are first and functions are last
				val.add((MemberObject) member);
			}
			Collections.sort(val);
			for( MemberObject member : val) {
				System.out.println(member.getName());
			}
			
			//fos.close();
		}
	}
}
