package hughes.jacob;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class Line extends JPanel{
	
	ArrayList<LineObject> lines;

	public Line() {
		lines = new ArrayList<>();
		setGUI();
	}
	
	public void addLine(LineObject lineObj) {
		lines.add(lineObj);
	}
	
	public ArrayList<LineObject> getLines(){
		return lines;
	}
	
	private void setGUI() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		//this.setBounds(20, 20, 950, 950);
		this.setBounds(0, 0, screenSize.width, screenSize.height);
		this.setBorder(BorderFactory.createEtchedBorder());
	}
	
	
    public void paint(Graphics g) {
    	super.paint(g);
		Graphics2D g2d = (Graphics2D) g; 
		//g2d.draw(new Line2D.Double(x+10,y+50,w+10,h+50));
		for(LineObject objs : lines) {
			g2d.setColor(objs.getColor());
			g2d.draw(new Line2D.Double(objs.getX1(),objs.getY1(),objs.getX2(),objs.getY2()));
		}
    }
    
    
}
