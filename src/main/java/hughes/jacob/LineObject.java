package hughes.jacob;

import java.awt.Color;

public class LineObject{
	private int x1;
	private int y1;
	private int x2;
	private int y2;
	private Color color = Color.BLACK;
	
	private DraggableUML source;
	private DraggableUML pointsTo;
	
	public LineObject(int _x1,int _y1,int _x2,int _y2,DraggableUML _source,DraggableUML _pointsTo) {
		x1 = _x1;
		y1 = _y1;
		x2 = _x2;
		y2 = _y2;
		setSource(_source);
		setPointsTo(_pointsTo);
	}
	
	public LineObject(DraggableUML _source,DraggableUML _pointsTo) {
		x1 = _source.getX();
		y1 = _source.getY();
		x2 = _pointsTo.getX();
		y2 = _pointsTo.getY();
		setSource(_source);
		setPointsTo(_pointsTo);
	}
	
	public void setColor(Color _color) {
		color = _color;
	}
	
	public Color getColor() {
		return color;
	}

	public int getX1() {
		return x1;
	}

	public int getY1() {
		return y1;
	}

	public int getX2() {
		return x2;
	}

	public int getY2() {
		return y2;
	}
	
	public void setX1(int val) {
		x1 = val;
	}

	public void setY1(int val) {
		y1 = val;
	}

	public void setX2(int val) {
		x2 = val;
	}

	public void setY2(int val) {
		y2 = val;
	}

	public DraggableUML getSource() {
		return source;
	}

	public void setSource(DraggableUML source) {
		this.source = source;
	}

	public DraggableUML getPointsTo() {
		return pointsTo;
	}

	public void setPointsTo(DraggableUML pointsTo) {
		this.pointsTo = pointsTo;
	}
}
