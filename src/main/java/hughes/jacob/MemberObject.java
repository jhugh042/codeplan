package hughes.jacob;

public class MemberObject implements Comparable<MemberObject>{

	String name;
	String description;
	MemberObject(){
		name = "";
		description = "";
	}
	MemberObject(String _name, String _desc){
		name = _name;
		description = _desc;
	}
	
	public String getBaseName() {
		return name;
	}
	
	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	
	
	@Override
	public String toString() {
		return getBaseName();
	}
	
	
	@Override
	public int compareTo(MemberObject o) {
		if(this.getName().contains("(") && o.getName().contains("(")) {
			return this.getName().compareTo(o.getName());
		}
		else if(this.getName().contains("(")) {
			return 1;
		}else {
			return -1;
		}
		//return 0;
	}
}
