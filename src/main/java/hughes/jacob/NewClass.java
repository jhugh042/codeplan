/*======================================================
 * Author: Jacob Hughes
 * Old Dominion Univeristy
 *///======================================================


package hughes.jacob;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/*
 * Used to create a dialog for adding a new UML class
 * TODO: edit List size so that adding a new member doesn't affect the dialog size
 */
@SuppressWarnings("serial")
public class NewClass extends JPanel{

	JButton newMember,removeMember;
	JTextField className,superName,classDesc;
	
	JList<MemberObject> pubMemList, protMemList, privMemList;
	
	final String memberCommand = "Add Class Member";
	final String removeMemberCommand = "Remove Class Member";
	
	DefaultListModel<MemberObject> publicMem;
	DefaultListModel<MemberObject> protectedMem;
	DefaultListModel<MemberObject> privateMem;
	
	NewClass(){
		className = new JTextField(10);
		superName = new JTextField(10);
		classDesc = new JTextField(10);
		publicMem = new DefaultListModel<MemberObject>();
		protectedMem = new DefaultListModel<MemberObject>();
		privateMem = new DefaultListModel<MemberObject>();
		
		setGUI();
	}
	
	NewClass(JTextField _className, JTextField _superName, JTextField _classDesc,
			DefaultListModel<MemberObject> _publicMem, DefaultListModel<MemberObject> _protectedMem, DefaultListModel<MemberObject> _privateMem){
		
		className = _className;
		superName = _superName;
		classDesc = _classDesc;
		publicMem = _publicMem;
		protectedMem = _protectedMem;
		privateMem = _privateMem;
		
		setGUI();
	}
	/*
	 * creates all the GUI objects and layout
	 */
	@SuppressWarnings("unchecked")
	private void setGUI() {
		//initialize the JLists
		pubMemList = new JList<MemberObject>(publicMem);
		protMemList = new JList<MemberObject>(protectedMem);
		privMemList = new JList<MemberObject>(privateMem);
		
		//make it so the list only displays the member name of the object
		pubMemList.setCellRenderer(new CustomList());
		protMemList.setCellRenderer(new CustomList());
		privMemList.setCellRenderer(new CustomList());
		
		//instead of grouping the lists i just cleared 
		//selection on the other two lists when a selection is made
		pubMemList.getSelectionModel().addListSelectionListener(e -> { 
			removeMember.setEnabled(true);
			protMemList.clearSelection();
			privMemList.clearSelection();
		});
		protMemList.getSelectionModel().addListSelectionListener(e->{
			removeMember.setEnabled(true);
			pubMemList.clearSelection();
			privMemList.clearSelection();
		});
		privMemList.getSelectionModel().addListSelectionListener(e->{
			removeMember.setEnabled(true);
			pubMemList.clearSelection();
			protMemList.clearSelection();
		});
		
		//initialize the buttons
		newMember = new ButtonController(memberCommand);
		removeMember = new ButtonController(removeMemberCommand);
		removeMember.setEnabled(false);
		
		//creates grid layout for dialog
		GridLayout grid = new GridLayout(0,6);
		setLayout(grid);
		grid.setHgap(0);
		grid.setVgap(10);
		
		//populates dialog with elements
		add(new JLabel("Class Name:"));
		add(className);
		add(new JLabel("Super Class Name:"));
		add(superName);
		add(new JLabel("Class Description:"));
		add(classDesc);
		add(new JLabel("Public Members :"));
		add(pubMemList);
		add(new JLabel("Protected Members :"));
		add(protMemList);
		add(new JLabel("Private Members :"));
		add(privMemList);
		add(new JLabel(""));
		add(new JLabel(""));
		add(newMember);
		add(removeMember);
		add(new JLabel(""));
		add(new JLabel(""));
	}
	
	/*
	 * creates button object and handles the buttons functions
	 */
	private class ButtonController extends JButton implements ActionListener{
		
		ButtonController(String commandName){

			addActionListener(this);
			setActionCommand(commandName);
			setText(commandName);
		}

		//Handles button controls
		public void actionPerformed(ActionEvent e) {
			//button for handling "Add class member"
			if(memberCommand.equals(e.getActionCommand())) {
				//initialize data here so that we can check it after "OK" is hit
				JTextField memberDescription = new JTextField(10);
				JTextField memberName = new JTextField(10);
				
				JRadioButton publicButton = new JRadioButton("Public");
				JRadioButton privateButton = new JRadioButton("Private");
				JRadioButton protectedButton = new JRadioButton("Protected");
				
				ButtonGroup group = new ButtonGroup();
				group.add(publicButton);
				group.add(privateButton);
				group.add(protectedButton);
				
				//generates JPanel with the initialized values
				JPanel addMember = new NewMember(memberDescription,memberName,
						publicButton,privateButton,protectedButton,group);
				
				//displays jpanel inside of option pane
				int result = JOptionPane.showConfirmDialog(null, 
						addMember, "Add a member to class",JOptionPane.OK_CANCEL_OPTION);
				
				//if ok is selected from option pane
				if(result == JOptionPane.OK_OPTION) {
					
					//check that all required fields have data
					if(memberName.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Member name field was empty", "Error", JOptionPane.OK_OPTION);
					}else {
						//adds data to list
						if(publicButton.isSelected()) {
							publicMem.addElement(new MemberObject(memberName.getText(),memberDescription.getText()));
							pubMemList.updateUI();
						}else if(protectedButton.isSelected()) {
							protectedMem.addElement(new MemberObject(memberName.getText(),memberDescription.getText()));
							protMemList.updateUI();
						}else if(privateButton.isSelected()) {
							privateMem.addElement(new MemberObject(memberName.getText(),memberDescription.getText()));
							privMemList.updateUI();
						}
					}
				}
			}//code that handles "remove class member" button
			else if(removeMemberCommand.equals(e.getActionCommand())) {
				
				//removes selected member or displays an error message
				if(!pubMemList.isSelectionEmpty()) {
					int response = JOptionPane.showConfirmDialog(null, "Remove: "+publicMem.getElementAt(pubMemList.getSelectedIndex()).toString()+"?");
					
					if(response==JOptionPane.OK_OPTION) {
						publicMem.removeElementAt(pubMemList.getSelectedIndex());
					}
				}
				else if(!protMemList.isSelectionEmpty()) {
					int response = JOptionPane.showConfirmDialog(null, "Remove: "+protectedMem.getElementAt(protMemList.getSelectedIndex()).toString()+"?");
					
					if(response==JOptionPane.OK_OPTION) {
						protectedMem.removeElementAt(protMemList.getSelectedIndex());
					}
				}
				else if(!privMemList.isSelectionEmpty()) {
					int response = JOptionPane.showConfirmDialog(null, "Remove: "+privateMem.getElementAt(privMemList.getSelectedIndex()).toString()+"?");
					
					if(response==JOptionPane.OK_OPTION) {
						privateMem.removeElementAt(privMemList.getSelectedIndex());
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "You havent selected a member to remove.", "Error", JOptionPane.ERROR_MESSAGE);
				}
				removeMember.setEnabled(false);
			}
		}	
	}
}
