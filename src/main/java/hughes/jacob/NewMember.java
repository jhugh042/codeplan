/*======================================================
 * Author: Jacob Hughes
 * Old Dominion Univeristy
 *///======================================================
package hughes.jacob;

import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class NewMember extends JPanel{

	//create class components
	JTextField memberDesc,memberName;
	JRadioButton publicButton, privateButton, protectedButton;
	ButtonGroup group;
	
	//default constructor is mostly used for testing layout
	NewMember(){
		
		publicButton = new JRadioButton("Public");
		privateButton = new JRadioButton("Private");
		protectedButton = new JRadioButton("Protected");
		memberDesc = new JTextField(10);
		memberName = new JTextField(10);
		
		setGUI();
	}
	

	NewMember(JTextField memDesc, JTextField memName,
			JRadioButton _publicButton,JRadioButton _privateButton,
			JRadioButton _protectedButton,ButtonGroup _group){
		
		//intialize compnonets
		publicButton = _publicButton;
		privateButton = _privateButton;
		protectedButton = _protectedButton;
		group = _group;
		memberDesc = memDesc;
		memberName = memName;
		
		setGUI();
	}
	
	//adds the components and layout to the dialog
	private void setGUI() {
		//creates grid layout for the dialog
		GridLayout grid = new GridLayout(0,2);
		setLayout(grid);
		
		//sets the components
		add(new JLabel("Class Member:"));
		add(memberName);
		add(new JLabel("Class Description:"));
		add(memberDesc);
		add(publicButton);
		add(new JLabel(""));
		add(privateButton);
		add(new JLabel(""));
		add(protectedButton);
	}
}
