package hughes.jacob;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@SuppressWarnings("serial")
public class Open extends ArrayList<ClassObject>{

	private String filename;
	Open(){
		JFileChooser c = new JFileChooser();
	    // Demonstrate "Open" dialog:
		c.setFileFilter(new FileNameExtensionFilter("XML Document","xml"));
		
	    int rVal = c.showOpenDialog(null);
	    if (rVal == JFileChooser.APPROVE_OPTION) {
	    	filename = c.getCurrentDirectory().toString()+"\\"
	    				+c.getSelectedFile().getName();
	    	if(!filename.contains(".xml")) {
	    		filename = filename+".xml";
	    	}
	    }
	    //System.out.println(filename);
	    readXML();
	}
	
	private void readXML() {
		
		File xmlFile = new File(filename);
		try {
			Document document = DocumentBuilderFactory.newInstance()
								.newDocumentBuilder().parse(xmlFile);
			document.getDocumentElement().normalize();
			
			//all nodes in Class
			NodeList classNodeList = document.getElementsByTagName("Class");
			
			for(int i =0; i<classNodeList.getLength();i++) {
				
				add(getClassObject(classNodeList.item(i)));
			}
			
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	private static ClassObject getClassObject(Node node) {
		
		String _class = null, _super = "", _desc = "";
		int x=0,y=0;
		
		DefaultListModel<MemberObject> _public= new DefaultListModel<MemberObject>(), 
				_private= new DefaultListModel<MemberObject>(), 
				_protected= new DefaultListModel<MemberObject>();;
		
		if(node.getNodeType() == Node.ELEMENT_NODE) {
			
			Element element = (Element) node;
			_class = getTagValue("Class_Name",element);
			_super = getTagValue("Super_Name",element);
			_desc = getTagValue("Class_Description",element);
			
			//loop through all class members
			NodeList childNodes = node.getChildNodes();
			for(int j=0;j<childNodes.getLength();j++) {
				if(childNodes.item(j).getNodeName().equals("Location")) {
					Element locationElement = (Element) childNodes.item(j);
					
					x = (int) Double.parseDouble(getTagValue("X",locationElement));
					y = (int) Double.parseDouble(getTagValue("Y",locationElement));
				}//get all the public members
				else if(childNodes.item(j).getNodeName().equals("Public")) {
					
					ArrayList<MemberObject> array = getMembers(childNodes,j);
					for(MemberObject member : array) {
						_public.addElement(member);
					}
				}//get all the private members
				else if(childNodes.item(j).getNodeName().equals("Private")) {
					
					ArrayList<MemberObject> array = getMembers(childNodes,j);
					for(MemberObject member : array) {
						_private.addElement(member);
					}
				}//get all the protected members
				else if(childNodes.item(j).getNodeName().equals("Protected")) {
					
					ArrayList<MemberObject> array = getMembers(childNodes,j);
					for(MemberObject member : array) {
						_protected.addElement(member);
					}
				}
			}
		}
		
		System.out.println(_class+":"+_super+"  Class Description:"+_desc);
		System.out.println("X: "+x+"  Y: "+y);
		ClassObject temporary =new ClassObject(_class,_super,_desc,_public,_private,_protected);
		temporary.setXY(x, y);
		return temporary;
	}
	
    private static String getTagValue(String tag, Element element) {
    	    	
        NodeList nodeList = element.getElementsByTagName(tag);
        if(nodeList.getLength()>0) {
        	return nodeList.item(0).getTextContent();
        }
        else {
        	return "";
        }

    }
    
    private static ArrayList<MemberObject> getMembers(NodeList nodes,int j){
		
		ArrayList<MemberObject> array = new ArrayList<MemberObject>();
		NodeList childChildNodes = nodes.item(j).getChildNodes();
		for(int z=0;z<childChildNodes.getLength();z++) {
			
			String memberName,memberDescription="";
			Element memberElement = (Element) childChildNodes.item(z);
			
			memberName = getTagValue("Member_Name",memberElement);
			memberDescription = getTagValue("Member_Description",memberElement);
			array.add(new MemberObject(memberName,memberDescription));
		}
    	return array;
    }
}
