package hughes.jacob;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Save{

	private String filename;
	Save(ArrayList<DraggableUML> array){

		JFileChooser fileChooser = new JFileChooser();
	    // Demonstrate "Open" dialog:
		fileChooser.setFileFilter(new FileNameExtensionFilter("XML Document","xml"));
		
	    int rVal = fileChooser.showSaveDialog(null);
	    if (rVal == JFileChooser.APPROVE_OPTION) {
	    	filename = fileChooser.getCurrentDirectory().toString()+"\\"
	    				+fileChooser.getSelectedFile().getName();
	    	if(!filename.contains(".xml")) {
	    		filename = filename+".xml";
	    	}
	    	generateXML(array);
	    }
	}
	
	public void generateXML(ArrayList<DraggableUML> array) {
		
		try {
			//create XML document
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
	        
            //sets root of XML as CodePlan
	        Element root = document.createElement("CodePlan");
	        document.appendChild(root);
	        
	        //for each uml availabale
			for(DraggableUML uml : array) {
				ClassObject obj = uml.getClassObject();
				
				//creats a class
		        Element classObj = document.createElement("Class");
		        root.appendChild(classObj);
				
		        /*
		         * add class name, super class name, class description, 
		         * location of uml, and all members of class
		         */
				Element className = document.createElement("Class_Name");
				className.appendChild(document.createTextNode(obj.getClassName()));
				classObj.appendChild(className);
				
				if(!(obj.getSuperName().equals("")) || obj.getSuperName().equals(null)) {
					Element superName = document.createElement("Super_Name");
					superName.appendChild(document.createTextNode(obj.getSuperName()));
					classObj.appendChild(superName);
				}
				
				if(!(obj.getClassDescription().equals("")) || obj.getClassDescription().equals(null)) {
					Element classDesc = document.createElement("Class_Description");
					classDesc.appendChild(document.createTextNode(obj.getClassDescription()));
					classObj.appendChild(classDesc);
				}
				
				Element location = document.createElement("Location");
				classObj.appendChild(location);
				
				Element x = document.createElement("X");
				x.appendChild(document.createTextNode(String.valueOf(uml.getLocation().getX())));
				location.appendChild(x);
				
				Element y = document.createElement("Y");
				y.appendChild(document.createTextNode(String.valueOf(uml.getLocation().getY())));
				location.appendChild(y);
				
				if(!obj.getPublicMembers().isEmpty()) {
					Element pub = document.createElement("Public");
					classObj.appendChild(pub);
					for(Object members: Arrays.asList(obj.getPublicMembers().toArray())) {
					
						Element memberElement = document.createElement("Member");
						pub.appendChild(memberElement);
					
						Element memberName = document.createElement("Member_Name");
						memberName.appendChild(document.createTextNode(((MemberObject) members).getName()));
						memberElement.appendChild(memberName);
				
						
						Element memberDesc = document.createElement("Member_Description");
						memberDesc.appendChild(document.createTextNode(((MemberObject) members).getDescription()));
						memberElement.appendChild(memberDesc);
					}
				}
				
				if(!obj.getPrivateMembers().isEmpty()) {
					Element priv = document.createElement("Private");
					classObj.appendChild(priv);
					for(Object members: Arrays.asList(obj.getPrivateMembers().toArray())) {
						
						Element memberElement = document.createElement("Member");
						priv.appendChild(memberElement);
						
						Element memberName = document.createElement("Member_Name");
						memberName.appendChild(document.createTextNode(((MemberObject) members).getName()));
						memberElement.appendChild(memberName);
						
						Element memberDesc = document.createElement("Member_Description");
						memberDesc.appendChild(document.createTextNode(((MemberObject) members).getDescription()));
						memberElement.appendChild(memberDesc);
					}
				}
				
				if(!obj.getProtectedMembers().isEmpty()) {
					Element prot = document.createElement("Protected");
					classObj.appendChild(prot);
					for(Object members: Arrays.asList(obj.getProtectedMembers().toArray())) {
						
						Element memberElement = document.createElement("Member");
						prot.appendChild(memberElement);
						
						Element memberName = document.createElement("Member_Name");
						memberName.appendChild(document.createTextNode(((MemberObject) members).getName()));
						memberElement.appendChild(memberName);
						
						Element memberDesc = document.createElement("Member_Description");
						memberDesc.appendChild(document.createTextNode(((MemberObject) members).getDescription()));
						memberElement.appendChild(memberDesc);
					}
				}
			}
			
			//generate the file
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(filename));
            
            transformer.transform(domSource, streamResult);

	        
		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
	}
}
